<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
     * error mode attributes copypasted from http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers#Error_Handling
	 * @throws PDOException
     */
    public function __construct($db = null) {  
	    if ($db) {
			$this->db = $db;
	    }
	    else {
		    try {
			    $this->db = new PDO('mysql:host=localhost;dbname=books;charset=utf8', 'root', '1234', array(PDO::ATTR_EMULATE_PREPARES => false, 
                                                                                                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		    }
		    catch(PDOException $error) {
			    echo $error->getMessage();
		    }
	    }
    }
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList() {	
	    $booklist = array();

	    try {
		$stmt = $this->db->query("SELECT * FROM book");
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($rows as $book) {
			$booklist[] = new Book($book['Title'], $book['Author'], $book['Description'], $book['id']);
		}
	    }

	    catch(PDOException $error) {
		    echo $error->getMessage();
	    }

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id) {
	    $book = null;
		
	    if(!filter_var($id, FILTER_VALIDATE_INT)) {		//Returns book as null if given ID is an invalid int
		    return null;
	    }

	    try {
		    $stmt = $this->db->prepare("SELECT * from book WHERE id=:id");
		    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
		    $stmt->execute();

		    $row = $stmt->fetch(PDO::FETCH_ASSOC);

		    if($row == false) { 			//Returns null if query did not find a book with a matching ID
			    return null;
		    }

		    $book = new Book($row['Title'], $row['Author'], $row['Description'], $row['id']); 
	    }

	    catch(PDOException $error) {
		 echo $error->getMessage();
	    }

        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book) {
	
	try {
		$stmt = $this->db->prepare("INSERT INTO book (Title, Author, Description)" . " VALUES(:Title, :Author, :Description)");
		$stmt->bindParam(':Title', $book->title, PDO::PARAM_STR);
		$stmt->bindParam(':Author', $book->author, PDO::PARAM_STR);
		$stmt->bindParam(':Description', $book->description, PDO::PARAM_STR);
		$stmt->execute();
		$book->id = $this->db->lastInsertId();
	}

	catch(PDOException $error) {
		 echo $error->getMessage();
	}	
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     */
    public function modifyBook($book) {
	try {
		$stmt = $this->db->prepare("UPDATE book SET Title=:Title, Author=:Author, Description=:Description WHERE id=:id");
		$stmt->bindParam(':Title', $book->title, PDO::PARAM_STR);
		$stmt->bindParam(':Author', $book->author, PDO::PARAM_STR);
		$stmt->bindParam(':Description', $book->description, PDO::PARAM_STR);
		$stmt->bindParam(':id', $book->id, PDO::PARAM_INT);
		$stmt->execute();
	}

	catch(PDOException $error) {
		 echo $error->getMessage();
	}

    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id) {
	try {
		$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
	}

	catch(PDOException $error) {
		echo $error->getMessage();
	}

    }
	
}

?>
